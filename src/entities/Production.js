import Tools, { isNotTerminal } from "./Tools";

export default class Production{
    constructor(leftSide, definition){
        this.firstNT=null;
        this.firstTerminal=null;
        this.leftSide=leftSide;
        this.definitionAsString=definition;
        this.definitionItems=[];
        this.voidableNT=null;
        this.composedJustByNTItems=null;
        this.completeInformation();
    }

    buildDefinitionItems(){
        // console.log(this.definitionAsString);
        const result = [];
        let cache = [];
        if(!this.definitionAsString) return;
        if(this.definitionAsString==="-|") {
            this.definitionItems=["-|"];
            return;
        };
        (this.definitionAsString.split("")).forEach((v, i) => {
            if(cache.length > 0){
                cache.push(v);
                if(v === ">"){
                    result.push(cache.join(""));
                    cache = [];
                }
                return;
            }
            if(v === "<"){
                if(cache.length> 0) throw "Texto es invalido";
                cache.push(v);
                return;
            }        
            result.push(v);
        });
        this.definitionItems=result;
        
    }

    completeInformation(){
        this.buildDefinitionItems();
        this.firstNT= this.definitionItems[0] === "-|" 
            ? null 
            : this.definitionItems[0];

        const terminalItems = this.definitionItems.filter(item => !isNotTerminal(item));
        
        this.composedJustByNTItems = (terminalItems.length === 0 && this.definitionItems.length > 0);

        this.firstTerminal=this.definitionItems[0];
    }

    isVoidable(){
        return this.definitionAsString==="-|";
    }

    getFirstNT(){
     return this.firstNT;
    }  

    writeDefinitionItemas(){
        console.log(this.definitionItems);
    }

    print(){
        console.log(`${this.leftSide} ==> ${this.definitionItems}`);
    }

}
