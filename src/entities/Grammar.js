
import Tools from "./Tools";
const Production = require("./Production");
const firstFollow = require('first-follow');


export default class Grammar {
    /**
     * 
     * @param {[Production]} productionList []
     * @param {*} options 
     * @param {String} options.auto generate information about avoiable productions, firstOnes and nextOnes by 
     *  each production and no-terminal automatically.
     */
    constructor(productionList, options = {}) {
        this.options = options;
        /** @type [Production] */
        this.productions = productionList;
        // this.avoidableProductions=[];
        this.type = null;
        this.noTerminalsAvoidable = [];
        // primeros de cada no terminal
        this.firstOnes = {};
        // siguientes de cada no terminal
        this.nextOnes = {};
        this.selectionOnes = {};
        // this.avoidableProduction = {};
        // primareros de cada produccion
        this.firstOnesByProductions = {};
        // Primeros por produccion
        this.firstOnesByProductions = [];
        this.selectionOnes = {};
        if (options.auto) {
            this.completeInformation();
        }
    }

    // creamos los primeros, siguientes y NO-TERMINALES anulables
    completeInformation() {
        this.noTerminalsAvoidable = this.calculateNtsVoidable();
        this.firstOnesByProductions = this.calculateFirstOnes();
        const rules = this.productions.map(p => ({
            left: p.leftSide,
            right: p.definitionItems.map(i => i === "-|" ? null : i)
        }));

        const { firstSets, followSets, predictSets } = firstFollow(rules);

        Object.keys(firstSets).forEach(fsKey => {
            this.firstOnes[fsKey] = firstSets[fsKey].filter(i => i != null);
        });

        Object.keys(followSets).forEach(fsKey => {
            this.nextOnes[fsKey] = followSets[fsKey].map(v => v === "\u0000" ? "-|" : v)
        });

        Object.keys(predictSets).forEach(psKey => {
            const newKey = parseInt(psKey) - 1;
            this.selectionOnes[newKey] = predictSets[psKey].map(v => v === "\u0000" ? "-|" : v);
        });
        this.type= this.identify();
        console.log(this.type)

    }

    /**
     * @typedef PrintOptions
     * @property {Boolean} options.nextOnes
     * @property {Boolean} options.firstOnes
     * @property {Boolean} options.selectionOnes
     * @param {PrintOptions} options
     */
    print(options = {}) {
        // console.log("------- INICIO ------");
        this.productions.forEach((p, i) => {
            console.log(`${i + 1} ${p.leftSide} ==> [${p.definitionItems}]`)
        });
        if (options.firstOnes) {
            console.log("----- PRIMEROS POR CADA NO TERMINAL ---------");
            console.log(this.firstOnes);
            console.log("---------------------------------------------");
        }
        if (options.nextOnes) {
            console.log("----- SIGUIENTES POR CADA NO TERMINAL ---------");
            console.log(this.nextOnes);
            console.log("---------------------------------------------");
        }
        if(options.selectionOnes){
            console.log("----- SELECCION POR CADA PRODUCCIÓN ---------");
            console.log(this.selectionOnes);
            console.log("---------------------------------------------");
        }
    }

    // calcula todos los no terminales anulables
    calculateNtsVoidable() {
        const directlyVoidableProductions = this.productions.filter(p => p.isVoidable())
            .map(p => p.leftSide);
        if (directlyVoidableProductions.length == 0) {
            return [];
        }

        const indirectlyVoidableProductions = [];
        // buscar las producciones anulables indirectamente en un nivel n;
        let avoidableFound = true;
        while (avoidableFound) {
            avoidableFound = false;
            const avoidableProductionsCandidates = this.productions
                // Obtengo las producciones que no son directamente anulables
                .filter(p => !directlyVoidableProductions.includes(p.leftSide))
                // Obtengo las producciones que no son indirectamente anulables
                .filter(p => !indirectlyVoidableProductions.includes(p.leftSide))
                // obtengo las producciones que estan compuestas solo por NO-TERMINALES
                .filter(p => p.composedJustByNTItems === true)
                // Obtengo las claves de las producciones no repetidas
                // TODO- en edicion
                .filter(p => this.checkNotTerminalVoidable(p.leftSide,
                    [...directlyVoidableProductions, ...indirectlyVoidableProductions])
                )
                .reduce((acc, p) => {
                    if (acc[p.leftSide]) {
                        acc[p.leftSide].push(p)
                    } else {
                        acc[p.leftSide] = [p];
                    }
                    return acc;
                }, {});

            if (Object.keys(avoidableProductionsCandidates).length > 0) {
                indirectlyVoidableProductions.push(...Object.keys(avoidableProductionsCandidates));
                avoidableFound = true;
            }
        }

        // console.log({
        //     directlyVoidableProductions,
        //     indirectlyVoidableProductions
        // });

        return [...directlyVoidableProductions, ...indirectlyVoidableProductions];
    }

    checkNotTerminalVoidable(nTerminal, voidableItems) {
        if (!nTerminal) throw "No terminal parameter is required"
        const voidableProductions = this.productions
            .filter(p => p.leftSide === nTerminal)
            // recorriendo las prodcciones
            .filter(p => {
                // console.log(`VERIFICANDO A ${p.leftSide} ==> ${p.definitionItems}`);                
                // dejamos pasar la que tengan algun elemento que sea anulable
                return p.definitionItems
                    .map(i => voidableItems.includes(nTerminal))
                    .filter(i => i === false).length > 0
            });

        // console.log(`VERIFICANDO LA NULIDAD DE ${nTerminal}==> ANULABLES ==> {${voidableItems}}`, { found: voidableProductions.length } );

        return voidableProductions.length > 0;
    }

    /**
     * return No terminal that is directly voidable 
     * @param {Boolean} justKey just leftSide
     */
    getAvoidableProductions(justKey = false) {
        return this.productions
            .map((p, i) => ({ index: i, production: p }) )
            .filter(indexVsProduction => indexVsProduction.production.isVoidable() )
            .map((indexVsProduction) => justKey 
                ? indexVsProduction.production.leftSide
                : indexVsProduction
            )
    }

    /// Identificar tipo de gramatica

    identify() {
        var aux = "";
        if (this.allFirstItemsTerminal() == 0) {
            aux = this.sonDisyuntosTerminales();

            if (aux == 0 && this.noTerminalsAvoidable.length == 0) {
                return "SS";
            } else {
                if(this.diferenciarGramaticas()=="Q"){
                    return "Q"
                }else if(this.diferenciarGramaticas()=="LL(1)"){
                    return "LL(1)"
                }
                
            }

        } else {
            aux = this.comprobarLL();

            if (aux == "LL(1)") {
                return "LL(1)";
            }
        }

        return "No cumple"
    }

    //si no cumple ninguna de las 3 gramaticas anteriores

    especificacion(){
        if( this.identify() == "No cumple" && this.allFirstItemsTerminal!==0 ){
            return `No cumple porque las producciones cuyo símbolo del lado izquiero es el mismo",
            tienes conjuntos de selección disyuntos`;
        }else if( this.identify() == "No CUMPLE" && this.voidableProductions.length!==0){
            return `El conjunto de selección de una producción no es disyunto con los terminales que tiene 
                el mismo simbolo izquierdo de esa producción`
        }else{
            return `Las produccione cuyo lado sean el mismo tienen el primero 
            elemento enla parte derecha esta repetido`
        }
    }


    //Diferenciar Gramaticas Q y LL()

    comprobarLL(){
        var aux=0;
        var i = 0;
        var n = 0;
        while (i < this.productions.length) {
            n = 0;
            while (n < this.productions.length) {
                var selecciones = this.selectionOnes[i];
                var seleccionesOtraProduccion = this.selectionOnes[n];
                if (this.productions[i].leftSide == this.productions[n].leftSide && i !== n) {
                    if (this.selectionOnes[i] == this.selectionOnes[n]) {
                        aux = 1;
                    }
                }
                n++;
            }
            i++;
        }

        //////////

        if (aux == 0) {
            return "LL(1)";
        }
        return ""
    }

    diferenciarGramaticas() {
        var aux = 0;
        var i = 0;
        var n = 0;
        // Gramatica Q

        let seleccionesActual = [];
        let primerosOtraProduccion = [];
        // WHILE
        while (i < this.productions.length) {
            seleccionesActual = this.selectionOnes[(i)];
            primerosOtraProduccion = [];
            n = 0;
            while (n < this.productions.length) {
                if (this.productions[i].leftSide == this.productions[n].leftSide && i !== n) {
                    primerosOtraProduccion.push([...this.firstOnesByProductions[n].firstItems]);
                }
                n++;


            }
            aux = Tools.comparaDisyuntos(seleccionesActual, primerosOtraProduccion)+aux;

            i++;

        }

        if(aux==0 && this.sonDisyuntosTerminales()==0){
            return "Q";
        }else {
            aux=this.comprobarLL();
            if(aux=="LL(1)"){
                return "LL(1)";
            }
        }

        return "hola";

    }

    //Difereciar terminales primeros con No terminales al lado izquierdo iguales

    sonDisyuntosTerminales() {
        var aux = 0
        var i = 0;
        var n = 0;
        // WHILE
        while (i < this.productions.length - 1) {
            n = i;
            while (n < this.productions.length - 1) {
                n++;
                if (this.productions[i].leftSide == this.productions[n].leftSide) {
                    if (this.productions[i].firstTerminal == this.productions[n].firstTerminal) {
                        return 1;
                    }
                }
            }
            i++;
        }

        return 0;

    }






    //comprobar si todos son terminales (primero del lado derecho)

    allFirstItemsTerminal() {
        var i = 0;
        this.productions.forEach(x => {
            if (Tools.isTerminal(x.firstTerminal) || x.firstTerminal == "-|") {
                i = i + 1;
            };
        });
        if (i == this.productions.length) {
            return 0;
        }
        return 1;
    }


    getDistincNoTerminals() {
        const leftSides = this.productions.map(p => p.leftSide);
        return [...new Set(leftSides)];
    }

    calculateFirstOnes() {
        // buscamos los primeros de cada produccion
        return this.productions.map((p, index) => {

            const itemsToCheck = [];
            p.definitionItems.every((item, index) => {
                if (item == "-|") return false
                itemsToCheck.push(item);
                if (Tools.isTerminal(item) || !this.noTerminalsAvoidable.includes(item)) {
                    return false
                }
                return true;
            });

            const firstList = itemsToCheck
                .map(item => Tools.isTerminal(item)
                    ? [item]
                    : this.findFirstList(item, [item])
                );

            // console.log(`Primeros(${index+1}): \t ${p.leftSide} ==> \t  [${p.definitionItems}] \t\t\t\t  PRIMEROS: { ${firstList} }`);

            return {
                index: index + 1,
                leftSide: p.leftSide,
                definitionItems: p.definitionItems,
                firstList: [...new Set(firstList)]
            }
        })
            .reduce((acc, value) => {


                const list = value.firstList
                    .map(sl => this.getItemsOnList(sl))
                    .reduce((acc, v) => { acc.push(...v); return acc }, []);

                acc.push({
                    index: value.index,
                    leftSide: value.leftSide,
                    firstItems: list
                });

                return acc;
            }, [])
            .map((item, i) => {
                item.firstItems = [...new Set(item.firstItems)];
                return item;
            });
    }

    /** Recursivo
    * Busca los primeros de un NT (NO-TERMINAL).
    * @param {String} noTerminal ejmeplo: <A> 
    * @param { [String] } itemOn
    */
    findFirstList(noTerminal, queuedItems) {
        // this.calls = this.calls + 1;
        // // console.log("------------>", this.calls);
        // if (this.calls > 500) {
        //     throw "LA pila se llano mk"
        // }
        let result = this.productions.filter(p => p.leftSide === noTerminal)

        // console.log("DATOS EN COLA ==>", JSON.stringify(queuedItems));
        // console.log(`---- findFirstList(${noTerminal}) PRODUCCIONES A VALIDAR`);
        // result.forEach(p => console.log(`${p.leftSide} ==> [${p.definitionItems}]`));
        const itemsToCheck = [];
        result = result.map(p => {
            p.definitionItems.every((item, index) => {
                if (item == "-|") return false

                itemsToCheck.push(item);
                if (Tools.isTerminal(item) || !this.noTerminalsAvoidable.includes(item)) {
                    return false
                }
                return true;
            });
        });

        const firstList = itemsToCheck
            .filter(i => i != noTerminal)
            .map(item => Tools.isTerminal(item)
                ? [item]
                : queuedItems.includes(item)
                    ? []
                    : this.findFirstList(item, [...queuedItems, item])
            )

        return firstList;

    }

    getItemsOnList(list) {
        if (Array.isArray(list)) {
            return list
                .map(i => this.getItemsOnList(i))
                .reduce((acc, v) => { acc.push(...v); return acc }, []);

        } else {
            return list
        }
    }

    isTypeSS() {
        const noneVoidable = this.noTerminalsAvoidable.length === 0;
        const allFirtItemsAreTerminals = this.productions
            .map(p => p.definitionItems[0])
            .filter(i => !Tools.isTerminal(i))
            .length == 0;
        if (!noneVoidable || !allFirtItemsAreTerminals) return false;

        // un mapa para saber que no-terminales tienen a un terminal
        const leftVsTerminalMap = this.productions
            .reduce((acc, production) => {
                const firstItem = production.definitionItems[0];
                const leftSide = production.leftSide;
                if (!acc[firstItem]) {
                    acc[firstItem] = [];
                }
                acc[firstItem].push(production.leftSide)
                return acc;
            }, {});
        console.log({ leftVsTerminalMap })

    }
}
