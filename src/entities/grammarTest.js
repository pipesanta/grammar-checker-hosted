const Grammar = require("./Grammar");
const Production = require("./Production");




const p1=new Production("<B>", "b<C>d");
const p2=new Production("<S>", "b<B>");
const p3=new Production("<C>", "b<C>d");
const p4=new Production("<C>", "a");
const p5=new Production("<D>", "b");

const p1=new Production("<S>", "a<A><B>b<C><D>");
const p2=new Production("<S>", "-|");
const p3=new Production("<A>", "<A><S>d");
const p4=new Production("<A>", "-|");
const p5=new Production("<B>", "<S><A>c");
const p6=new Production("<B>", "e<C>");
const p7=new Production("<B>", "-|");
const p8=new Production("<C>", "<S>f");
const p9=new Production("<C>", "<C>g");
const p10=new Production("<C>", "-|");
const p11=new Production("<D>", "a<B><D>");
const p12=new Production("<D>", "-|");


//const g1=new Grammar([p1,p2,p3,p4,p5,p6,p7,p8,p9,p10]);
// const g1=new Grammar([p1,p2,p3,p4,p5]);
// console.log(g1.identify());
//console.log("NO TERMINALES ANULABLES ==>", g1.noTerminalsAvoidable.length);


function probarLaExtraccionDePrimeros1(){

    const p1=new Production("<A>", "a<B><C>");
    const p2=new Production("<A>", "<D>b<A>");
    const p3=new Production("<B>", "-|");
    const p4=new Production("<B>", "b<A><B>");
    const p5=new Production("<C>", "c<C>");
    const p6=new Production("<C>", "<D>d<B>");
    const p7=new Production("<D>", "-|");
    const p8=new Production("<D>", "e<E>");
    const p9=new Production("<E>", "<B><D>");
    const p10=new Production("<A>", "f");

    const gramatica1 = new Grammar([p1, p2, p3, p4, p5, p6, p7, p8, p9, p10]);
   

}

// probarLaExtraccionDePrimeros1();

function probarLaExtraccionDePrimeros2(){
    
    const grama = new Grammar([
        new Production("<S>", "a<B>c"),
        new Production("<S>", "<A>a"),
        new Production("<A>", "b<A>"),
        new Production("<A>", "<C><D>a"),
        new Production("<B>", "<C><D>b"),
        new Production("<B>", "-|"),
        new Production("<C>", "<C>b"),
        new Production("<C>", "-|"),
        new Production("<D>", "<A>b<D>"),
        new Production("<D>", "-|")
    ]);

    console.log("NO-TERMINALES ANULABLES => ", grama.noTerminalsAvoidable);
}

// probarLaExtraccionDePrimeros2();

function probarLaExtraccionDePrimeros3(){
    const grama = new Grammar([
        new Production("<S>", "<A><B><C><D>"),  // 1
        new Production("<A>", "a<A>p"),         // 2
        new Production("<A>", "-|"),            // 3
        new Production("<B>", "<B>q"),          // 4
        new Production("<B>", "b"),             // 5
        new Production("<C>", "<A><F>"),        // 6
        new Production("<C>", "<C>"),           // 7
        new Production("<D>", "d<D>r"),         // 8
        new Production("<D>", "-|"),            // 9
        new Production("<F>", "f<F>g"),         // 10
        new Production("<F>", "-|"),            // 11
    ]);
}
//probarLaExtraccionDePrimeros3();







