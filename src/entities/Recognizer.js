import StackAutomata from "./StackAutomata";
import Stack from "./Stack";

export default class Recognizer {

    constructor(stackAutomata) {
        /**@type {StackAutomata} */
        this.stackAutomata = stackAutomata;
        this.stack = new Stack();
        this.steps=[];
        this.accepted = false;
    }

    recognizeText(inputText) {
        const inputTextByCharacters = inputText.split("");
        const lastTwoCharacters = inputTextByCharacters.slice(inputTextByCharacters.length - 2).join("");

        if (lastTwoCharacters !== "-|") {
            return ({ error: "end_simbol_missing" })
        }

        this.stack.items = [...this.stackAutomata.initialSetup];
        const textToEvalue = inputTextByCharacters.slice(0, inputTextByCharacters.length - 2);

        try {
            [...textToEvalue, lastTwoCharacters].forEach(char => this.processChar(char) );
        } catch (error) {
            console.log(error);            
        }

        return ({ steps: this.steps, accepted: this.accepted });

    }

    processChar(char){
        const itemOnTop = this.stack.getTop();
        const { fn, text } = this.stackAutomata.transitions[itemOnTop][char] || {};
        const step = { 
            stack: JSON.stringify(this.stack.items),
            input: char, action: text || "RECHACE", 
            description: !fn ? `"${char}" no es una entrada valida para el tope en la pila "${itemOnTop}"` : ''
        };

        this.steps.push(step);
        if(!fn){
            throw "Symbol not allowed"
        }

        const response = fn.call(this.stack, itemOnTop);
        if(response === "KEEP"){
            return this.processChar(char);
        }
        this.accepted = (response === "ACCEPT");
    }

}
