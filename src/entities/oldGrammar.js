const Production = require("./Production");
const Tools = require("./Tools");
const firstFollow = require('first-follow');


class OldGrammar {
    /**
     * 
     * @param {[Production]} productionList []
     * @param {*} options 
     * @param {String} options.auto generate information about avoiable productions, firstOnes and nextOnes by 
     *  each production and no-terminal automatically.
     */
    constructor(productionList, options = {}) {
        this.options = options;
        /** @type [Production] */
        this.productions = productionList;
        // this.avoidableProductions=[];
        this.type = null;
        this.nextOnes = {};
        this.leftNT = [];
        this.noTerminalsAvoidable = [];
        this.calls = 0;
        this.firstOnesByProductions = [];
        if (options.auto) {
            this.completeInformation();
        }
    }

    completeInformation() {

        // this.noTerminalsAvoidable = this.calculateNtsVoidable();
        // this.firstOnes = this.calculateFirstOnes();
        // this.buildLeftNT();
        // this.initialSymbol = this.leftNT[0];
        // this.getNextOnes();

        const rules = this.productions.map(p => ({
            left: p.leftSide,
            right: p.definitionItems.map(i => i=="-|" ? null : i )
        }));

        const { firstSets, followSets, predictSets } = firstFollow(rules);

        this.firstOnes = 


        

    }

    setNoTerminalsAvoidable() {
        this.noTerminalsAvoidable = this.calculateNtsVoidable();
    }

    setFirstOnes() {
        this.firstOnes = this.calculateFirstOnes();
    }

    /**
     * @typedef PrintOptions
     * @property {Boolean} options.firstOnesByProduction
     * @property {Boolean} options.firstOnesByNoTerminal
     * @param {PrintOptions} options
     */
    print(options = {}) {
        // console.log("------- INICIO ------");
        this.productions.forEach((p, i) => {
            console.log(`${i + 1} ${p.leftSide} ==> [${p.definitionItems}]`)
        });
        if (options.firstOnesByNoTerminal) {
            console.log("----- PRIMEROS POR CADA NO TERMINAL ---------");
            this.getDistincNoTerminals().forEach(i => {
                console.log(`--  Primeros(${i}): {${this.getFirstByNoTerminal(i)}}`)
            });
            console.log("---------------------------------------------");
        }
        if (options.firstOnesByProduction) {
            console.log("----- PRIMEROS POR CADA PRODUCTION ---------");
            this.firstOnesByProductions.forEach((item, i) => {
                console.log(`--  Primeros(${item.index}): {${item.firstItems}}`)
            });
            console.log("---------------------------------------------");
        }
        // console.log(`No terminales anulables => `, this.noTerminalsAvoidable);
        // console.log("------- FIN ------");
    }

    // calcula todos los no terminales anulables
    calculateNtsVoidable() {
        const directlyVoidableProductions = this.productions.filter(p => p.isVoidable())
            .map(p => p.leftSide);
        if (directlyVoidableProductions.length == 0) {
            return [];
        }

        const indirectlyVoidableProductions = [];
        // buscar las producciones anulables indirectamente en un nivel n;
        let avoidableFound = true;
        while (avoidableFound) {
            avoidableFound = false;
            const avoidableProductionsCandidates = this.productions
                // Obtengo las producciones que no son directamente anulables
                .filter(p => !directlyVoidableProductions.includes(p.leftSide))
                // Obtengo las producciones que no son indirectamente anulables
                .filter(p => !indirectlyVoidableProductions.includes(p.leftSide))
                // obtengo las producciones que estan compuestas solo por NO-TERMINALES
                .filter(p => p.composedJustByNTItems === true)
                // Obtengo las claves de las producciones no repetidas
                // TODO- en edicion
                .filter(p => this.checkNotTerminalVoidable(p.leftSide,
                    [...directlyVoidableProductions, ...indirectlyVoidableProductions])
                )
                .reduce((acc, p) => {
                    if (acc[p.leftSide]) {
                        acc[p.leftSide].push(p)
                    } else {
                        acc[p.leftSide] = [p];
                    }
                    return acc;
                }, {});

            // console.log(">>>>>>> CANDIDATOS PARA SER ANULABLES >>>>>>>>>")
            // console.log(Object.keys(avoidableProductionsCandidates));
            // console.log("<<<<<<<<<<<<<<<<<")

            // const avoidableProductions = Object.keys(avoidableProductionsCandidates)
            //     .filter(itemKey => {
            //         const voidableDefinitionMap = avoidableProductionsCandidates[itemKey]
            //             .map(prod => { 
            //                 // listado de booleanos que indican si cada uno de las producciones en la definicion son anulables o no.
            //                 const voidableitemMap = prod.definitionItems.map(def => {
            //                     return directlyVoidableProductions.includes(def) || indirectlyVoidableProductions.includes(def);
            //                 })
            //                 return voidableitemMap.includes(false) ? false : true;
            //             })      
            //         return voidableDefinitionMap.includes(false) ? false : true;                   
            //     })

            // console.log({avoidableProductions});


            if (Object.keys(avoidableProductionsCandidates).length > 0) {
                indirectlyVoidableProductions.push(...Object.keys(avoidableProductionsCandidates));
                avoidableFound = true;
            }
        }

        // console.log({
        //     directlyVoidableProductions,
        //     indirectlyVoidableProductions
        // });

        return [...directlyVoidableProductions, ...indirectlyVoidableProductions];
    }

    calculateFirstOnes() {
        // console.log("CALCULANDO LOS PRIMEROS DE CADA PRODUCCION");
        // console.log(`ANULABLES ==> [${this.noTerminalsAvoidable}]`);
        const result = {};
        // buscamos los primeros de cada produccion
        this.firstOnesByProductions = this.productions.map((p, index) => {

            const itemsToCheck = [];
            p.definitionItems.every((item, index) => {
                if (item == "-|") return false
                itemsToCheck.push(item);
                if (Tools.isTerminal(item) || !this.noTerminalsAvoidable.includes(item)) {
                    return false
                }
                return true;
            });

            const firstList = itemsToCheck
                .map(item => Tools.isTerminal(item)
                    ? [item]
                    : this.findFirstList(item, [item])
                );

            // console.log(`Primeros(${index+1}): \t ${p.leftSide} ==> \t  [${p.definitionItems}] \t\t\t\t  PRIMEROS: { ${firstList} }`);

            return {
                index: index + 1,
                leftSide: p.leftSide,
                definitionItems: p.definitionItems,
                firstList: [...new Set(firstList)]
            }
        })
            .reduce((acc, value) => {
                const list = value.firstList
                    .map(sl => this.getItemsOnList(sl))
                    .reduce((acc, v) => { acc.push(...v); return acc }, []);

                acc.push({
                    index: value.index,
                    leftSide: value.leftSide,
                    firstItems: list
                });
                return acc;
            }, [])
            .map((item, i) => {
                item.firstItems = [...new Set(item.firstItems)];
                return item;
            })

    }

    getItemsOnList(list) {
        if (Array.isArray(list)) {
            return list
                .map(i => this.getItemsOnList(i))
                .reduce((acc, v) => { acc.push(...v); return acc }, []);

        } else {
            return list
        }
    }

    checkNotTerminalVoidable(nTerminal, voidableItems) {
        if (!nTerminal) throw "No terminal parameter is required"
        const voidableProductions = this.productions
            .filter(p => p.leftSide === nTerminal)
            // recorriendo las prodcciones
            .filter(p => {
                // dejamos pasar la que tengan algun elemento que sea anulable
                return p.definitionItems
                    .map(i => voidableItems.includes(nTerminal))
                    .filter(i => i === true).length > 0
            });

        // console.log(`VERIFICANDO LA NULIDAD DE ${nTerminal}==> ANULABLES ==> {${voidableItems}}`, { found: voidableProductions.length } );

        return voidableProductions.length > 0;
    }

    /** Recursivo
     * Busca los primeros de un NT (NO-TERMINAL).
     * @param {String} noTerminal ejmeplo: <A> 
     * @param { [String] } itemOn
     */
    findFirstList(noTerminal, queuedItems) {
        this.calls = this.calls + 1;
        // console.log("------------>", this.calls);
        if (this.calls > 500) {
            throw "LA pila se llano mk"
        }
        let result = this.productions.filter(p => p.leftSide === noTerminal)

        // console.log("DATOS EN COLA ==>", JSON.stringify(queuedItems));
        // console.log(`---- findFirstList(${noTerminal}) PRODUCCIONES A VALIDAR`);
        // result.forEach(p => console.log(`${p.leftSide} ==> [${p.definitionItems}]`));
        const itemsToCheck = [];
        result = result.map(p => {
            p.definitionItems.every((item, index) => {
                if (item == "-|") return false

                itemsToCheck.push(item);
                if (Tools.isTerminal(item) || !this.noTerminalsAvoidable.includes(item)) {
                    return false
                }
                return true;
            });
        });

        const firstList = itemsToCheck
            .filter(i => i != noTerminal)
            .map(item => Tools.isTerminal(item)
                ? [item]
                : queuedItems.includes(item)
                    ? []
                    : this.findFirstList(item, [...queuedItems, item])
            )

        return firstList;

    }

    buildLeftNT() {
        this.productions.forEach((p) => {
            this.leftNT.push(p.leftSide);
        });
    }

    calculateNextOnes(){
        return this.getDistincNoTerminals()
            .map(nt => ({key: nt, list: this.findNextOnesSantaList(nt, [nt])}))
    }

    findNextOnesSantaList(noTerminal, queuedItems) {
        this.calls = this.calls + 1;
        if(this.calls>20){
            throw "LA PILA SE LLENO MK"
        }
        console.log('BUSCANDO SIGUIENTE DE', noTerminal, { queuedItems });
        /* ESPERADO ==> {
            <A>: ["a", "b"],
            <B>: []
        }*/
        
        if(Tools.isTerminal(noTerminal)){
            console.log(`${noTerminal} es un terminal`);
            
            return [noTerminal]
        }
        // if(queuedItems.includes(noTerminal)) return [];


        this.productions
            // filtro las producciones que tienen el no terminal en su definicion
            .filter(p => p.definitionItems.includes(noTerminal))
            .map(p => {               
                const list = [];
                if(p.definitionItems == ["-|"]){
                    
                }
                p.definitionItems.every(item => {
                    list.push(item);
                    if( Tools.isTerminal(item) || !this.noTerminalsAvoidable.includes(item) ){
                        return false;
                    }
                    return true;
                });

                console.log("BUSCAMOS EN LA LISTA", list);

                const result = list.map(i => this.findNextOnesSantaList(i, [ ...queuedItems, i ]));

                console.log("RESULTADO DE BUSQUEDA ", result);                

                if (this.productions[0].leftSide === noTerminal) {
                    return [...result, "-|"];
                }
                return result
            })
    }

    getNextOnes() {

        var unique = this.leftNT.filter(Tools.onlyUnique);
        console.log(unique.length);

        unique.forEach((u) => {
            this.productions.forEach((p, n) => {
                p.definitionItems.forEach((d, i) => {
                    if (d == u) {

                        this.buildNextOnes(u, p, i, [u]);
                        // console.log("el no terminal", u, "está en ", p.definitionItems);
                        // console.log("posición en items", i, p.definitionItems[i]);
                        // console.log("Tamaño =>", p.definitionItems.length)
                    }
                });

            });

        });

        console.log("FINALIZO PUTOS ===> Los siguientes son", this.nextOnes)
    }

    buildNextOnes(body, produccion, numeroItem, queuedItems) {

        console.log("COMO VAN LOS SIGUIENTES", this.nextOnes);                
        console.log(`----> SIGUEINTES(${body}) EN-ESPERA: ${queuedItems}`); 

        // this.calls = this.calls+1;
        if (body == undefined) {
            return;
        }
        // if(this.calls > 50){
        //     throw "Se LLENO LA PILA"
        // }
        
        
        
        
        //console.log(this.initialSymbol);
        //console.log("body =>", body);
        //console.log(produccion.definitionItems);

        const keyExist = this.nextOnes[body];
        if (body == this.initialSymbol) {
            if (keyExist) {
                this.nextOnes[body] = [...new Set([...this.nextOnes[body], "-|" ])];
            } else {
                this.nextOnes[body] = ['-|']
            }
        }
        // si estamos buscando los sigueintes del ultimo item de las definiciones
        if (numeroItem == produccion.definitionItems.length - 1) {

            if(produccion.leftSide === body){
                return;
            }

            // console.log("El no terminal ", body, "está de último");
            // console.log(produccion.leftSide);
            this.productions.forEach((p, n) => {
                p.definitionItems.forEach((d, posicionItem) => {
                    if (d == produccion.leftSide) {


                        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
                        if(queuedItems.includes(body)){

                            const indexes = queuedItems.map((v,i) => ({ index: i, value: v }));
                            const firstItem = indexes.find(i => i.index == 0);
                            const ocurrences = indexes.filter(i => i.value == firstItem.value );


                            if(ocurrences.length > 2){

                                const delta = ocurrences[1].index - ocurrences[0].index;

                                const matches = [];

                                for (let index = 0; index < 3 * delta; index= index+delta) {
                                    matches.push(queuedItems[index] === firstItem.value);                         
                                }

                                if(!matches.includes(false)){
                                    
                                    let comunItems = [];
                                    for (let index = delta; index > 0; index--) {
                                        

                                        console.log(`AGREGAR DATOS DE ${queuedItems[index]} `);
                                        comunItems = [ ...comunItems, ...(this.nextOnes[queuedItems[index]] || [])  ]
                                        
                                    }

                                    for (let index = delta; index > 0; index--) {
                                        const key = this.nextOnes[queuedItems[index]];
                                        console.log(key);
                                        
                                        this.nextOnes[key] = [ ...(this.nextOnes[key] || []), ...comunItems ];
                                    }
                                    return;
                                    
                                    
                                    // throw "DEPENDENCIA INFINITA"

                                }

                            }

                            
                  
                        }
                        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$                        
                        this.buildNextOnes(produccion.leftSide, p, posicionItem, [...queuedItems, produccion.leftSide]);
                    }
                });
            });
            // console.log("ahfijsodhjosdhj", this.nextOnes, body);
            if(this.nextOnes[body]){
                this.nextOnes[body]=[
                    ...new Set(this.nextOnes[body]),
                    ...new Set(this.nextOnes[produccion.leftSide])];
            }else{
                this.nextOnes[body]=[ ...new Set(this.nextOnes[produccion.leftSide]) ];
            }

        }else {
            // estamos buscando los siguientes de uno que no es el ultimo

            
            const nextItem = produccion.definitionItems[numeroItem + 1];
            console.log({ nextItem, definitions: produccion.definitionItems });
            
            if (Tools.isTerminal(nextItem)) {
                if (keyExist) {
                    this.nextOnes[body]=[
                        ...new Set( this.nextOnes[body] ),
                        ...new Set( nextItem)];
                    
                } else {                    
                    this.nextOnes[body] = [...new Set(nextItem)];
                }
            }

            else{
                // este es un NO-TERMINAL y hay que verificar si es anulable o no
                // const firstItemsOfNoTerminal = this.getFirstByNoTerminal(nextItem);
                // si el NO-TERMINAL es un anulable

                const firstItemsOnCache = [];

                const subList = produccion.definitionItems.slice(numeroItem+1);
                subList.every(def => {
                    // si el elemento siguiente es un NO-TERMINAL anulable o un terminal
                    const isNoTerminal = Tools.isNotTerminal(def);
                    const isVoidable = isNoTerminal && this.noTerminalsAvoidable.includes(def);

                    console.log({ isNoTerminal, isVoidable });
                    
                    // si es un terminal
                    if( !isNoTerminal){
                        firstItemsOnCache.push(def);
                        return false;
                    }
                    // si es un NO-TERMINAL y no es anulable
                    if(!isVoidable){
                        firstItemsOnCache.push( ...this.getFirstByNoTerminal(def)  );
                        return false;
                    }
                    if(isNoTerminal && isVoidable && subList[subList.length -1] == def  ){
                        console.log("ACA ES DONDE NOS FALTA  .l. (-_-) .l. ");

                        this.productions.forEach((p, n) => {
                            p.definitionItems.forEach((d, posicionItem) => {
                                if (d == produccion.leftSide) {  
                                    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
                                    if(queuedItems.includes(body)){
                                        if(queuedItems[0] == queuedItems[2]){
                                            // console.log("COMO VAN LOS SIGUIENTES", this.nextOnes);                
                                            // console.log(`----> SIGUEINTES(${body}) EN-ESPERA: ${queuedItems}`);                                
                                            this.nextOnes[queuedItems[1]] = [ ...this.nextOnes[queuedItems[0]] ];
                                            return;
                                        }                            
                                    }
                                    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
                                    // console.log(this.nextOnes);
                                    this.buildNextOnes(produccion.leftSide, p, posicionItem, [...queuedItems, produccion.leftSide]);
                                }
                            });
                        });

                        return false;

                    }
                    return true;
                });

                console.log({firstItemsOnCache, SIG: this.nextOnes});
                

                if(!this.nextOnes[body]){
                    this.nextOnes[body] = [ ...new Set(firstItemsOnCache)]

                }else{
                    this.nextOnes[body] = [
                        ...new Set([
                            ...this.nextOnes[body],
                            ...firstItemsOnCache
                        ])
                    ]
                }

                
            }
        }

        // al fin eliminamos los repetidos

        const result = Object.keys(this.nextOnes).map(k => {
            const list = [...new Set(this.nextOnes[k])];
            return ({
                key: k,
                list: list
            })
        }).reduce((acc, v) => {
            acc[v.key] = v.list;
            return acc;
        }, {});

        this.nextOnes = result;

    }

    // construir 
    findNextOnes(noTerminal) {

    }


    getAvoidableProductions(justKey = false) {
        return this.productions.filter(p => p.isVoidable())
            .map(p => justKey ? p.leftSide : p)
    }

    /// Identificar tipo de gramatica

    identify() {
        if (this.allFirstItemsTerminal()) {
            var aux = 0;
            var i = 0;
            var n = 0;
            while (i < this.productions.length - 1) {
                n = i;
                while (n < this.productions.length - 1) {
                    n++;
                    if (this.productions[i].leftSide == this.productions[n].leftSide) {
                        if (this.productions[i].firstNT == this.productions[n].firstNT) {
                            aux = 1;
                        }
                    }
                }
                i++;

            }

            if (aux == 0) {
                if (this.noTerminalsAvoidable == 0) {
                    return "SS";
                } else {
                    return "Q";
                }
            }


        };

    }

    //comprobar si todos son terminales (primero del lado derecho)

    allFirstItemsTerminal() {
        var i = 0;
        this.productions.forEach(x => {
            if (Tools.isTerminal(x.firstNT)) {
                i = i + 1;
            };
        });

        return i == this.productions.length;
    }


    /**
     * 
     * @param {String} noTerminal 
     * @returns {[String]}
     */
    getFirstByNoTerminal(noTerminal) {
        
        const list = this.firstOnesByProductions
            .filter(item => item.leftSide === noTerminal)
            .reduce((acc, value) => {
                acc.push(...value.firstItems);
                return acc;
            }, []);
        console.log(`PRIMEROS(${noTerminal}) = {${[...new Set(list)]}}`);
        
        return [...new Set(list)];
    }

    getDistincNoTerminals() {
        const leftSides = this.productions.map(p => p.leftSide);
        return [...new Set(leftSides)];
    }

}



module.exports = OldGrammar;