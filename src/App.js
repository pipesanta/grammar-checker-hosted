import React from 'react';
import GrammarLayout from './components/GrammarLayout';
import './App.css';

function App() {
  return (
   <main>
      <GrammarLayout/>
    </main>
  );
}

export default App;
